package com.example.student.todolist;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

public class ToDoFragment extends Fragment {

    private RecyclerView recyclerView;
    private ActivityCallback activityCallback;
    private MainActivity activity;

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        this.activity = (MainActivity)activity;
        activityCallback = (ActivityCallback)activity;
    }

    @Override
    public void onDetach() {
        super.onDetach();
        activityCallback = null;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_todo_list, container, false);

        recyclerView = (RecyclerView) view.findViewById(R.id.recyclerView);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));

        ToDoItem item1 = new ToDoItem("Hello", "oct", "nov", "k");
        ToDoItem item2 = new ToDoItem("It's me", "aug", "dec", "c");
        ToDoItem item3 = new ToDoItem("How are you?", "jan", "feb", "l");
        ToDoItem item4 = new ToDoItem("Goodbye", "march", "apr", "o");

        activity.todoItems.add(item1);
        activity.todoItems.add(item2);
        activity.todoItems.add(item3);
        activity.todoItems.add(item4);

        return view;
    }

}