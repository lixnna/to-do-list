package com.example.student.todolist;


import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.app.Fragment;

public class ItemFragment extends Fragment{
    private MainActivity activity;
    public TextView tv1;
    public TextView tv2;
    public TextView tv3;
    public TextView tv4;

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        this.activity = (MainActivity)activity;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View layoutView = inflater.inflate(R.layout.fragment_todo_list, container, false);

        tv1 = (TextView)layoutView.findViewById(R.id.textView1);
        tv2 = (TextView)layoutView.findViewById(R.id.textView2);
        tv3 = (TextView)layoutView.findViewById(R.id.textView3);
        tv4 = (TextView)layoutView.findViewById(R.id.textView4);

        tv1.setText("Name: " + activity.todoItems.get(activity.currentItem).title);
        tv2.setText("Name: " + activity.todoItems.get(activity.currentItem).title);
        tv3.setText("Name: " + activity.todoItems.get(activity.currentItem).title);
        tv4.setText("Name: " + activity.todoItems.get(activity.currentItem).title);

        return layoutView;
    }
}
