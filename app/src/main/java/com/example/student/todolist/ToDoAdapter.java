package com.example.student.todolist;

import android.support.v7.widget.RecyclerView;

import java.util.ArrayList;

public abstract class ToDoAdapter extends RecyclerView.Adapter<ToDoHolder> {
    private ArrayList<ToDoItem> todoItems;

    public ToDoAdapter(ArrayList<ToDoItem> todoItems) {
        this.todoItems = todoItems;
    }

}