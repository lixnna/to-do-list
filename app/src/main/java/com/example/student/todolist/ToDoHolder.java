package com.example.student.todolist;

import android.support.v7.widget.RecyclerView;
import android.view.View;

public abstract class ToDoHolder extends RecyclerView.ViewHolder{
    public ToDoHolder(View itemView) {
        super(itemView);
    }
}
