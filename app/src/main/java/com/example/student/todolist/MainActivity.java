package com.example.student.todolist;

import android.app.FragmentTransaction;
import android.app.Fragment;

import java.util.ArrayList;

public abstract class MainActivity extends SingleFragmentActivity implements ActivityCallback{
    public int currentItem;
    public ArrayList<ToDoItem> todoItems = new ArrayList<>();

    @Override
    protected Fragment createFragment() {
        return new Fragment();
    }

    @Override
    public void onPostSelected(int pos) {
        currentItem = pos;
        Fragment newFragment = new ItemFragment();
        FragmentTransaction transaction = getFragmentManager().beginTransaction();

        transaction.replace(R.id.fragment_container, newFragment);
        transaction.addToBackStack(null);

        transaction.commit();
    }
}
