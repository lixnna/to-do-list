package com.example.student.todolist;

public class ToDoItem {
    public String title;
    public String dateAdded;
    public String dateDue;
    public String work;

    public ToDoItem(String title, String dateAdded, String dateDue, String work) {
        this.title = title;
        this.dateAdded = dateAdded;
        this.dateDue = dateDue;
        this.work = work;
    }
}
